##############################################################################
# Type:     Cryo Systems
# Project:  C3S
# DevType:  C3S-:CRYO: INTLCK - Interlocks
# Author:	Fenyvesi Gabor
# Date:		18-11-2019
# Version:  v1.0
# UPDATE : Dominik Domagala  version 1.1 

############################
#  STATUS BLOCK
############################ 
define_status_block()

#Vacuum
add_digital("BeamVacOK",					PV_DESC="Beam Vacuum status",				PV_ONAM="OK",			PV_ZNAM="NOK")    
add_digital("IsoVacOK",					    PV_DESC="Insulation Vacuum status",			PV_ONAM="OK",			PV_ZNAM="NOK")
add_digital("CMIsoVacOK",					PV_DESC="Insulation Vacuum status",			PV_ONAM="OK",			PV_ZNAM="NOK")


##CTL vacuum OK

add_digital("CTLVacOK1",					PV_DESC="CTL PT891 status",			        PV_ONAM="OK",			PV_ZNAM="NOK")
add_digital("CTLVacOK2",					PV_DESC="CTL PT892 status",			        PV_ONAM="OK",			PV_ZNAM="NOK")
add_digital("EBVacOK",					    PV_DESC="EndBox VGC070 status",			    PV_ONAM="OK",			PV_ZNAM="NOK")

#PSS
add_digital("PressureHeGuardOK",			PV_DESC="Pressure of He-Guard status",		PV_ONAM="OK",			PV_ZNAM="NOK")      

#RF
add_digital("RFON",							PV_DESC="RF state",							PV_ONAM="ON",			PV_ZNAM="OFF")  

#Water
add_digital("WaterOK",						PV_DESC="Water status",						PV_ONAM="OK",			PV_ZNAM="NOK")



############################
#  COMMAND BLOCK
############################ 
define_command_block()

add_digital("cmd_RFEnable",					PV_DESC="CMD: Enable RF")
add_digital("cmd_RFDisable",				PV_DESC="CMD: Enable RF")
add_digital("cmd_CavVacEnable",				PV_DESC="CMD: Enable cavvac")
add_digital("cmd_CavVacDisable",			PV_DESC="CMD: disable Cavvac")
add_digital("cmd_IsoVacEnable",				PV_DESC="CMD: Enable Isovac")
add_digital("cmd_IsoVacDisable",			PV_DESC="CMD: disable Isovac")

############################
# Parameter block
############################
#define_parameter_block()

add_analog("CavityVac1",                "REAL",  PV_PREC="2",               ARCHIVE=" 1Hz",              PV_DESC="Value of VGC1000",                 PV_EGU="mBar")
add_analog("CavityVac2",                "REAL",  PV_PREC="2",               ARCHIVE=" 1Hz",              PV_DESC="Value of VGC2000",                 PV_EGU="mBar")
add_analog("CavityVac3",                "REAL",  PV_PREC="2",               ARCHIVE=" 1Hz",              PV_DESC="Value of VGC3000",                 PV_EGU="mBar")
add_analog("CavityVac4",                "REAL",  PV_PREC="2",               ARCHIVE=" 1Hz",              PV_DESC="Value of VGC4000",                 PV_EGU="mBar")
add_analog("CMGaugeIsoVac1",            "REAL",  PV_PREC="2",               ARCHIVE=" 1Hz",              PV_DESC="Value of VGC01100",                 PV_EGU="mBar")
add_analog("CMGaugeIsoVac2",            "REAL",  PV_PREC="2",               ARCHIVE=" 1Hz",              PV_DESC="Value of VGP01100",                 PV_EGU="mBar")
add_digital("CDSVacOK",					PV_DESC="CDS vacuum ok confirmation status",			PV_ONAM="OK",			PV_ZNAM="NOK")
